//
//  main.swift
//  My First Project
//
//  Created by Administrador on 08/02/16.
//  Copyright © 2016 ITESM. All rights reserved.
//

import Foundation

let answer = randomIntBetween(1, high: 100)
var check = ""
var turn = 1

if(answer%2==0){
    check = "pair"
}else {
    check = "lone"
}

while(true)  {
    
    print("Guess #\(turn): Enter a number between 1 and 100.")
    
    let userInput = input()
    
    let inputAsInt = Int(userInput)
    if let guess = inputAsInt  {
        
        if(guess > answer) {
            print("Lower!")
        } else if(guess < answer) {
            print("Higher!")
        } else {
            print("Correct! The answer was \(answer) and it is a \(check) number.")
            break
        }
        
    } else  {
        print("Invalid input! Please enter a number.")
        continue
    }
    
    turn = turn + 1
    
}

print("It took you \(turn) tries.")